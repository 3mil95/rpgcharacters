﻿using System;
using RPGCharacters.Attribute;
using RPGCharacters.Items;


namespace RPGCharacters.Characters
{

    public class Warrior : Character
    {
        static readonly String ClassName = "Warrior";
        static readonly ArmorTypes[] UsableArmorTypes = new ArmorTypes[] { ArmorTypes.ARMOUR_MAIL, ArmorTypes.ARMOUR_PLATE };
        static readonly WeaponTypes[] UsableWeaponTypes = new WeaponTypes[] { WeaponTypes.WEAPON_AXE, WeaponTypes.WEAPON_HAMMER, WeaponTypes.WEAPON_SWORD };
        static readonly PrimaryAttribute AttributeIncreasePerLevel = new PrimaryAttribute() { Vitality = 5, Strength = 3, Dexterity = 2, Intelligence = 1 };
        static readonly PrimaryAttribute StartAttributes = new PrimaryAttribute() { Vitality = 10, Strength = 5, Dexterity = 2, Intelligence = 1 };


        public Warrior(String name) : base()
        {
            this.Name = name;
            BasePrimaryAttributes = new PrimaryAttribute(StartAttributes);
            TotalPrimaryAttributes = new PrimaryAttribute();
            UpdateTotalPrimaryAttributes();
            SecondaryAttributes.UpdateAttributes(TotalPrimaryAttributes);
        }

        public override string ToString()
        {
            return $@"+---------------------------------------------------------------------------------+
 {ClassName}   {base.ToString()}
";
        }

        public override void LevelUp(int numbeOfLevels)
        {
            for (int i = 0; i < numbeOfLevels; i++)
            {
                BasePrimaryAttributes.AddToAttributes(AttributeIncreasePerLevel);
            }
            base.LevelUp(numbeOfLevels);
        }

        protected override ArmorTypes[] GetUsableArmorTypes()
        {
            return UsableArmorTypes;
        }

        protected override WeaponTypes[] GetUsableWeaponTypes()
        {
            return UsableWeaponTypes;
        }

        public override double GetDps()
        {
            return base.GetDps() * (1 + (double)TotalPrimaryAttributes.Strength / 100);
        }
    }
}
