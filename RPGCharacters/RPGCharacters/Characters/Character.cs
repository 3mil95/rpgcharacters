﻿using System;
using RPGCharacters.Attribute;
using RPGCharacters.Items;
using RPGCharacters.Exceptions;
using System.Collections.Generic;
using System.Text;




namespace RPGCharacters.Characters
{
    public abstract class Character
    {
        public string Name { protected set; get; } = "";
        public int Level { protected set; get; } = 1;
        public PrimaryAttribute BasePrimaryAttributes { protected set; get; }
        public PrimaryAttribute TotalPrimaryAttributes { protected set; get; }
        public SecondaryAttribute SecondaryAttributes { protected set; get; }
        public Dictionary<Slots, Item> Equipments { protected set; get; }
        
        /// <summary>
        /// Creats a RPG Character.
        /// </summary>
        protected Character()
        {
            Equipments = new Dictionary<Slots, Item>();
            SecondaryAttributes = new SecondaryAttribute();
        }

        public override string ToString()
        {
            return @$"Name: {Name} Level: {Level}
-----------------------------------------------------------------------------------
 Attributes:
-----------------------------------------------------------------------------------
   {TotalPrimaryAttributes}
   {SecondaryAttributes} DPS: {Math.Round(GetDps(), 2)}

 items:
-----------------------------------------------------------------------------------
{GetItems()}
+---------------------------------------------------------------------------------+
";
        }

        /// <summary>
        /// Makes a string that contains all the equiped items.
        /// </summary>
        /// <returns>The string</returns>
        public string GetItems()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Item item in Equipments.Values)
            {
                sb.Append($"   {item}\n");
            }
            return sb.ToString();
        }


        /// <summary>
        /// Level up the character.
        /// </summary>
        public void LevelUp()
        {
            LevelUp(1);
        }

        /// <summary>
        /// Level up the character.
        /// </summary>
        /// <param name="numbeOfLevels">The number of levels.</param>
        /// <exception cref="ArgumentException"> If the numbeOfLevels is less then 1 </exception>
        public virtual void LevelUp(int numbeOfLevels)
        {
            if (numbeOfLevels < 1)
            {
                throw new ArgumentException("numbeOfLevels must be greater than 0");
            }
            Level += numbeOfLevels;
            UpdateTotalPrimaryAttributes();
            SecondaryAttributes.UpdateAttributes(TotalPrimaryAttributes);
        }


        /// <summary>
        /// Updates TotalPrimaryAttributes through adds the base attributes with and the attributes from equipment.
        /// </summary>
        protected void UpdateTotalPrimaryAttributes()
        {
            TotalPrimaryAttributes.SetAttributes(BasePrimaryAttributes);
            
            foreach (Item item in Equipments.Values)
            {
                if (item is Armor armor)
                {
                    TotalPrimaryAttributes.AddToAttributes(armor.Attributes);
                }
            }
        }


        /// <summary>
        /// Checks if the armor can be used by the character.
        /// </summary>
        /// <param name="armor">The armor.</param>
        /// <returns>If the character can use the armor.</returns>
        private bool IsEquipebel(Armor armor)
        {
            foreach (ArmorTypes armorTypes in GetUsableArmorTypes())
            {
                if (armor.ArmorType == armorTypes)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks if the weapon can be used by the character.
        /// </summary>
        /// <param name="weapon">The weapon.</param>
        /// <returns>If the character can use the weapon.</returns>
        private bool IsEquipebel(Weapon weapon)
        {
            foreach (WeaponTypes weaponTypes in GetUsableWeaponTypes())
            {
                if (weapon.WeaponType == weaponTypes)
                {
                    return true;
                }
            }
            return false;
        }


        /// <summary>
        /// Equip the character with an item.
        /// </summary>
        /// <param name="item">The item with which the character is to be equipped.</param>
        /// <returns>A success message</returns>
        /// <exception cref="InvalidWeaponException">If the wepon is not equipebel</exception>
        /// <exception cref="InvalidArmorException">If the armor is not equipebel</exception>
        /// <exception cref="InvalidItemException">If item is not a wepon or armor</exception>
        public string EquipItem(Item item)
        {
            if (item is Armor armor)
            {
                return EquipItem(armor);
            } else if (item is Weapon weapon)
            {
                return EquipItem(weapon);
            }
            throw new InvalidItemException("Can not equip this type of item!");
        }

        /// <summary>
        /// Equip the character with an weapon.
        /// </summary>
        /// <param name="weapon">The weapon with which the character is to be equipped.</param>
        /// <returns>A success message</returns>
        /// <exception cref="InvalidWeaponException">If the wepon is not equipebel</exception>
        private string EquipItem(Weapon weapon)
        {
            if (Level < weapon.ItemLevel)
            {
                throw new InvalidWeaponException("Too low level to use this item!");
            }
            if (!IsEquipebel(weapon))
            {
                throw new InvalidWeaponException("Can not used this weapon type!");
            }
            Equipments.Remove(weapon.ItemSlot);
            Equipments.Add(weapon.ItemSlot, weapon);
            return "New weapon equipped!";
        }

        /// <summary>
        /// Equip the character with an armor.
        /// </summary>
        /// <param name="armor">The armor with which the character is to be equipped.</param>
        /// <returns>A success message</returns>
        /// <exception cref="InvalidArmorException">If the armor is not equipebel</exception>
        private string EquipItem(Armor armor)
        {
            if (Level < armor.ItemLevel)
            {
                throw new InvalidArmorException("Too low level to use this item!");
            }
            if (!IsEquipebel(armor))
            {
                throw new InvalidArmorException("Can not used theis armor type!");
            }
            Equipments.Remove(armor.ItemSlot);
            Equipments.Add(armor.ItemSlot, armor);
            UpdateTotalPrimaryAttributes();
            SecondaryAttributes.UpdateAttributes(TotalPrimaryAttributes);
            return "New armour equipped!";
        }


        /// <summary>
        /// Calculates the character's damage per second.
        /// </summary>
        /// <returns>The character's damage per second.</returns>
        public virtual double GetDps()
        {
            if (Equipments.ContainsKey(Slots.SLOT_WEAPON))
            {
                if (Equipments[Slots.SLOT_WEAPON] is Weapon weapon)
                {
                    return weapon.GetWeaponDps();
                }
            }
            return 1;
        }


        /// <summary>
        /// Return the armor type the character can use.
        /// </summary>
        /// <returns>The armor types.</returns>
        protected abstract ArmorTypes[] GetUsableArmorTypes();

        /// <summary>
        /// Return the weapon type the character can use.
        /// </summary>
        /// <returns>The weapon types.</returns>
        protected abstract WeaponTypes[] GetUsableWeaponTypes();
    }
}
