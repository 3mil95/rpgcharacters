﻿using System;
using RPGCharacters.Attribute;
using RPGCharacters.Items;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Characters
{
    public class Mage : Character
    {
        static readonly String ClassName = "Mage";
        static readonly ArmorTypes[] UsableArmorTypes = new ArmorTypes[] { ArmorTypes.ARMOUR_CLOTH };
        static readonly WeaponTypes[] UsableWeaponTypes = new WeaponTypes[] { WeaponTypes.WEAPON_STAFF, WeaponTypes.WEAPON_WAND };
        static readonly PrimaryAttribute AttributeIncreasePerLevel = new PrimaryAttribute() { Vitality = 3, Strength = 1, Dexterity = 1, Intelligence = 5 };
        static readonly PrimaryAttribute StartAttributes = new PrimaryAttribute() { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };


        public Mage(String name) : base()
        {
            this.Name = name;
            BasePrimaryAttributes = new PrimaryAttribute(StartAttributes);
            TotalPrimaryAttributes = new PrimaryAttribute();
            UpdateTotalPrimaryAttributes();
            SecondaryAttributes.UpdateAttributes(TotalPrimaryAttributes);
        }

        public override string ToString()
        {
            return $@"+---------------------------------------------------------------------------------+
 {ClassName}   {base.ToString()}
";
        }

        public override void LevelUp(int numbeOfLevels)
        {
            for (int i = 0; i < numbeOfLevels; i++)
            {
                BasePrimaryAttributes.AddToAttributes(AttributeIncreasePerLevel);
            }
            base.LevelUp(numbeOfLevels);
        }

        protected override ArmorTypes[] GetUsableArmorTypes()
        {
            return UsableArmorTypes;
        }

        protected override WeaponTypes[] GetUsableWeaponTypes()
        {
            return UsableWeaponTypes;
        }

        public override double GetDps()
        { 
            return base.GetDps() * (1 + (double)TotalPrimaryAttributes.Intelligence / 100);
        }
    }
}
