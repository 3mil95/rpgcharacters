﻿using System;
using RPGCharacters.Attribute;
using RPGCharacters.Items;

namespace RPGCharacters.Characters
{
    public class Rogue : Character
    {
        static readonly String ClassName = "Rogue";
        static readonly ArmorTypes[] UsableArmorTypes = new ArmorTypes[] { ArmorTypes.ARMOUR_LEATHER, ArmorTypes.ARMOUR_MAIL };
        static readonly WeaponTypes[] UsableWeaponTypes = new WeaponTypes[] { WeaponTypes.WEAPON_DAGGER, WeaponTypes.WEAPON_SWORD };
        static readonly PrimaryAttribute AttributeIncreasePerLevel = new PrimaryAttribute() { Vitality = 3, Strength = 1, Dexterity = 4, Intelligence = 1 };
        static readonly PrimaryAttribute StartAttributes = new PrimaryAttribute() { Vitality = 8, Strength = 2, Dexterity = 6, Intelligence = 1 };


        public Rogue(String name) : base()
        {
            this.Name = name;
            BasePrimaryAttributes = new PrimaryAttribute(StartAttributes);
            TotalPrimaryAttributes = new PrimaryAttribute();
            UpdateTotalPrimaryAttributes();
            SecondaryAttributes.UpdateAttributes(TotalPrimaryAttributes);
        }

        public override string ToString()
        {
            return $@"+---------------------------------------------------------------------------------+
 {ClassName}   {base.ToString()}
";
        }

        public override void LevelUp(int numbeOfLevels)
        {
            for (int i = 0; i < numbeOfLevels; i++)
            {
                BasePrimaryAttributes.AddToAttributes(AttributeIncreasePerLevel);
            }
            base.LevelUp(numbeOfLevels);
        }


        protected override ArmorTypes[] GetUsableArmorTypes()
        {
            return UsableArmorTypes;
        }

        protected override WeaponTypes[] GetUsableWeaponTypes()
        {
            return UsableWeaponTypes;
        }

        public override double GetDps()
        {
            return base.GetDps() * (1 + (double)TotalPrimaryAttributes.Dexterity / 100);
        }
    }
}
