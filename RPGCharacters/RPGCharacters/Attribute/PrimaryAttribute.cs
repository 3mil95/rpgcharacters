﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attribute
{
    public class PrimaryAttribute
    {
        public int Strength { get; set; } = 0;
        public int Dexterity { get; set; } = 0;
        public int Intelligence { get; set; } = 0;
        public int Vitality { get; set; } = 0;

        public override string ToString()
        {
            return $"Vit: {Vitality} Str: {Strength} Dex: {Dexterity} Int: {Intelligence}";
        }

        public PrimaryAttribute()
        {
        }

        public PrimaryAttribute(PrimaryAttribute attribute)
        {
            Strength = attribute.Strength;
            Dexterity = attribute.Dexterity;
            Intelligence = attribute.Intelligence;
            Vitality = attribute.Vitality;
        }

        public override bool Equals(object obj)
        {
            if (obj is PrimaryAttribute attribute)
            {
                return (Strength == attribute.Strength &&
                        Dexterity == attribute.Dexterity &&
                        Intelligence == attribute.Intelligence &&
                        Vitality == attribute.Vitality);
            }
            return base.Equals(obj);
        }

        public static bool operator ==(PrimaryAttribute lhv, PrimaryAttribute rhv)
        {
            return (lhv.Strength == rhv.Strength && 
                    lhv.Dexterity == rhv.Dexterity && 
                    lhv.Intelligence == rhv.Intelligence &&
                    lhv.Vitality == rhv.Vitality);
        }

        public static bool operator !=(PrimaryAttribute lhv, PrimaryAttribute rhv)
        {
            return !(lhv.Strength == rhv.Strength &&
                    lhv.Dexterity == rhv.Dexterity &&
                    lhv.Intelligence == rhv.Intelligence &&
                    lhv.Vitality == rhv.Vitality);
        }

        /// <summary>
        /// Add to the attribute values.
        /// </summary>
        /// <param name="attribute">The amount by which the attributes increase.</param>
        public void AddToAttributes(PrimaryAttribute attribute)
        {
            Strength += attribute.Strength;
            Dexterity += attribute.Dexterity;
            Intelligence += attribute.Intelligence;
            Vitality += attribute.Vitality;
        }

        /// <summary>
        /// Set the attribute values.
        /// </summary>
        /// <param name="attribute">The values the attributes should have.</param>
        public void SetAttributes(PrimaryAttribute attribute)
        {
            Strength = attribute.Strength;
            Dexterity = attribute.Dexterity;
            Intelligence = attribute.Intelligence;
            Vitality = attribute.Vitality;
        }
    }
}
