﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attribute
{
    public class WeaponAttribute
    {   
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }

        public override string ToString()
        {
            return $"Damage: {Damage} AttackSpeed: {AttackSpeed}";
        }
    }
}
