﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attribute
{
    public class SecondaryAttribute
    {
        public int Health { get; set; } = 0;
        public int ArmorRating { get; set; } = 0;
        public int ElementalResistance { get; set; } = 0;

        public override string ToString()
        {
            return $"Health: {Health} Armor Rating: {ArmorRating} Elemental Resistance: {ElementalResistance}";
        }

        public override bool Equals(object obj)
        {
            if (obj is SecondaryAttribute attribute)
            {
                return (Health == attribute.Health &&
                        ArmorRating == attribute.ArmorRating &&
                        ElementalResistance == attribute.ElementalResistance);
            }
            return base.Equals(obj);
        }

        /// <summary>
        /// Recalculates the secondary attributes.
        /// </summary>
        /// <param name="primaryAttribute">The primary attribute used to recalculate the secondary attributes.</param>
        public void UpdateAttributes(PrimaryAttribute primaryAttribute)
        {
            Health = primaryAttribute.Vitality * 10;
            ArmorRating = primaryAttribute.Strength + primaryAttribute.Dexterity;
            ElementalResistance = primaryAttribute.Intelligence;
        }
    }
}
