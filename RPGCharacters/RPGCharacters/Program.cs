﻿using System;
using RPGCharacters.Characters;
using RPGCharacters.Items;
using RPGCharacters.Attribute;

namespace RPGCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            Character lase = new Mage("lase");


            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = Slots.SLOT_BODY,
                ArmorType = ArmorTypes.ARMOUR_CLOTH,
                Attributes = new PrimaryAttribute() { Vitality = 2, Strength = 1 }
            };

            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 2,
                ItemSlot = Slots.SLOT_HEAD,
                ArmorType = ArmorTypes.ARMOUR_CLOTH,
                Attributes = new PrimaryAttribute() { Vitality = 1, Intelligence = 5 }
            };

            Armor testClothLeg = new Armor()
            {
                ItemName = "Common cloth head leg",
                ItemLevel = 1,
                ItemSlot = Slots.SLOT_LEGS,
                ArmorType = ArmorTypes.ARMOUR_CLOTH,
                Attributes = new PrimaryAttribute() { Vitality = 4, Intelligence = 1 }
            };

            Armor testLeg = new Armor()
            {
                ItemName = "Common leg",
                ItemLevel = 1,
                ItemSlot = Slots.SLOT_LEGS,
                ArmorType = ArmorTypes.ARMOUR_CLOTH,
                Attributes = new PrimaryAttribute() { Vitality = 2, Intelligence = 3 }
            };

            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slots.SLOT_WEAPON,
                WeaponType = WeaponTypes.WEAPON_WAND,
                Attributes = new WeaponAttribute() { Damage = 12, AttackSpeed = 0.8 }
            };

            Console.WriteLine(lase);
            lase.EquipItem(testBow);

            //Console.WriteLine(lase);
            lase.LevelUp();
            Console.WriteLine(lase);
            Console.WriteLine(lase.TotalPrimaryAttributes);

            /*lase.EquipItem(testPlateBody);
            lase.EquipItem(testClothHead);
            lase.EquipItem(testClothLeg);
            

            Console.WriteLine(lase);

            lase.EquipItem(testLeg);

            Console.WriteLine(lase);*/
        }
    }
}
