﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Exceptions
{
    public class InvalidItemException : Exception
    {
        public InvalidItemException()
        {
        }

        public InvalidItemException(string message)
            : base(message)
        {
        }

        public InvalidItemException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
