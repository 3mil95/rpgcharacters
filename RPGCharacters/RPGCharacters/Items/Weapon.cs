﻿using System;
using RPGCharacters.Attribute;

namespace RPGCharacters.Items
{
    public enum WeaponTypes
    {
        WEAPON_AXE,
        WEAPON_BOW,
        WEAPON_DAGGER,
        WEAPON_HAMMER,
        WEAPON_STAFF,
        WEAPON_SWORD,
        WEAPON_WAND
    }

    public class Weapon : Item
    {
        public WeaponTypes WeaponType { get; set; }
        public WeaponAttribute Attributes { get; set; }

        public override string ToString()
        {
            return $"{base.ToString()} Type: {WeaponType} - {Attributes}";
        }


        /// <summary>
        /// Calculates the weapon's damage per second.
        /// </summary>
        /// <returns>The weapon's damage per second.</returns>
        public double GetWeaponDps()
        {
            return Attributes.Damage * Attributes.AttackSpeed;
        }
    }
}
