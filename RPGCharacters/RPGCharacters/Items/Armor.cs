﻿using System;
using RPGCharacters.Attribute;

namespace RPGCharacters.Items
{
    public enum ArmorTypes
    {
        ARMOUR_CLOTH,
        ARMOUR_LEATHER,
        ARMOUR_MAIL,
        ARMOUR_PLATE
    }  

    public class Armor : Item
    {
        public ArmorTypes ArmorType { get; set; } 
        public PrimaryAttribute Attributes { get; set; }

        public override string ToString()
        {
            return $"{base.ToString()} Type: {ArmorType} - {Attributes}"; 
        }
    }
}
