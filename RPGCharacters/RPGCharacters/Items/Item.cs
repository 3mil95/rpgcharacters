﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace RPGCharacters.Items
{
    public enum Slots
    {
        SLOT_HEAD,
        SLOT_BODY,
        SLOT_LEGS,
        SLOT_WEAPON,
    }

    public abstract class Item
    {
        public String ItemName { get; set; } = "";
        public int ItemLevel { get; set; } = 1;
        public Slots ItemSlot { get; set; }

        public override string ToString()
        {
            return $"{ItemSlot}: {ItemName}";
        }
    }
}
