using System;
using Xunit;
using RPGCharacters.Characters;
using RPGCharacters.Attribute;

namespace RPGCharactersTests
{
    public class CharactersTests
    {
        [Fact]
        public void Character_CaractarCreated_IsLevelOne()
        {
            // Arrange
            Character testCharacter = new Mage("test");
            int expected = 1;
            // Act
            int actuel = testCharacter.Level;
            // Assert
            Assert.Equal(expected, actuel);
        }

        [Fact]
        public void LevelUp_CaractarCreatedLvelsUp_IsLevelTwo()
        {
            // Arrange
            Character testCharacter = new Mage("test");
            int expected = 2;
            // Act
            testCharacter.LevelUp();
            int actuel = testCharacter.Level;
            // Assert
            Assert.Equal(expected, actuel);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_LevelIncreaseWithZeroOrLessLevels_ThrowArgumentException(int numbeOfLevels)
        {
            // Arrange
            Character testCharacter = new Mage("test");
            // Act and Assert
            Assert.Throws<ArgumentException>(() => testCharacter.LevelUp(numbeOfLevels));
        }

        [Fact]
        public void Mage_MageCreated_MageHasStartingAttributes()
        {
            // Arrange
            Character testCharacter = new Mage("test");
            PrimaryAttribute expected = new PrimaryAttribute() { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };
            // Act
            PrimaryAttribute actuel = testCharacter.TotalPrimaryAttributes;
            // Assert
            Assert.Equal(expected, actuel);
        }

        [Fact]
        public void Ranger_RangerCreated_RangerHasStartingAttributes()
        {
            // Arrange
            Character testCharacter = new Ranger("test");
            PrimaryAttribute expected = new PrimaryAttribute() { Vitality = 8, Strength = 1, Dexterity = 7, Intelligence = 1 };
            // Act
            PrimaryAttribute actuel = testCharacter.TotalPrimaryAttributes;
            // Assert
            Assert.Equal(expected, actuel);
        }

        [Fact]
        public void Rogue_RogueCreated_RogueHasStartingAttributes()
        {
            // Arrange
            Character testCharacter = new Rogue("test");
            PrimaryAttribute expected = new PrimaryAttribute() { Vitality = 8, Strength = 2, Dexterity = 6, Intelligence = 1 };
            // Act
            PrimaryAttribute actuel = testCharacter.TotalPrimaryAttributes;
            // Assert
            Assert.Equal(expected, actuel);
        }

        [Fact]
        public void Warrior_WarriorCreated_WarriorHasStartingAttributes()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            PrimaryAttribute expectedAttribute = new PrimaryAttribute() { Vitality = 10, Strength = 5, Dexterity = 2, Intelligence = 1 };
            string expected = $"{expectedAttribute.Vitality} {expectedAttribute.Strength} {expectedAttribute.Dexterity} {expectedAttribute.Intelligence}";
            // Act
            PrimaryAttribute actuelAttribute = testCharacter.TotalPrimaryAttributes;
            string actuel = $"{actuelAttribute.Vitality} {actuelAttribute.Strength} {actuelAttribute.Dexterity} {actuelAttribute.Intelligence}";
            // Assert
            Assert.Equal(expected, actuel);
        }

        [Fact]
        public void LevelUp_MageLevelUp_AttributesIncreaseAsExpected()
        {
            // Arrange
            Character testCharacter = new Mage("test");
            PrimaryAttribute startAttribute = new PrimaryAttribute() { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };
            PrimaryAttribute AttributeIncreasePerLevel = new PrimaryAttribute() { Vitality = 3, Strength = 1, Dexterity = 1, Intelligence = 5 };
            PrimaryAttribute expected = new PrimaryAttribute() { Vitality = startAttribute.Vitality + AttributeIncreasePerLevel.Vitality,
                                                                          Strength = startAttribute.Strength + AttributeIncreasePerLevel.Strength, 
                                                                          Dexterity = startAttribute.Dexterity + AttributeIncreasePerLevel.Dexterity, 
                                                                          Intelligence = startAttribute.Intelligence + AttributeIncreasePerLevel.Intelligence
            };
            // Act
            testCharacter.LevelUp();
            PrimaryAttribute actuel = testCharacter.TotalPrimaryAttributes;
            // Assert
            Assert.Equal(expected, actuel);
        }

        [Fact]
        public void LevelUp_RangerLevelUp_AttributesIncreaseAsExpected()
        {
            // Arrange
            Character testCharacter = new Ranger("test");
            PrimaryAttribute startAttribute = new PrimaryAttribute() { Vitality = 8, Strength = 1, Dexterity = 7, Intelligence = 1 };
            PrimaryAttribute AttributeIncreasePerLevel = new PrimaryAttribute() { Vitality = 2, Strength = 1, Dexterity = 5, Intelligence = 1 };
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Vitality = startAttribute.Vitality + AttributeIncreasePerLevel.Vitality,
                Strength = startAttribute.Strength + AttributeIncreasePerLevel.Strength,
                Dexterity = startAttribute.Dexterity + AttributeIncreasePerLevel.Dexterity,
                Intelligence = startAttribute.Intelligence + AttributeIncreasePerLevel.Intelligence
            };
            // Act
            testCharacter.LevelUp();
            PrimaryAttribute actuel = testCharacter.TotalPrimaryAttributes;
            // Assert
            Assert.Equal(expected, actuel);
        }

        [Fact]
        public void LevelUp_RogueLevelUp_AttributesIncreaseAsExpected()
        {
            // Arrange
            Character testCharacter = new Rogue("test");
            PrimaryAttribute startAttribute = new PrimaryAttribute() { Vitality = 8, Strength = 2, Dexterity = 6, Intelligence = 1 };
            PrimaryAttribute AttributeIncreasePerLevel = new PrimaryAttribute() { Vitality = 3, Strength = 1, Dexterity = 4, Intelligence = 1 };
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Vitality = startAttribute.Vitality + AttributeIncreasePerLevel.Vitality,
                Strength = startAttribute.Strength + AttributeIncreasePerLevel.Strength,
                Dexterity = startAttribute.Dexterity + AttributeIncreasePerLevel.Dexterity,
                Intelligence = startAttribute.Intelligence + AttributeIncreasePerLevel.Intelligence
            };
            // Act
            testCharacter.LevelUp();
            PrimaryAttribute actuel = testCharacter.TotalPrimaryAttributes;
            // Assert
            Assert.Equal(expected, actuel);
        }

        [Fact]
        public void LevelUp_WarriorLevelUp_AttributesIncreaseAsExpected()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            PrimaryAttribute startAttribute = new PrimaryAttribute() { Vitality = 10, Strength = 5, Dexterity = 2, Intelligence = 1 };
            PrimaryAttribute AttributeIncreasePerLevel = new PrimaryAttribute() { Vitality = 5, Strength = 3, Dexterity = 2, Intelligence = 1 };
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Vitality = startAttribute.Vitality + AttributeIncreasePerLevel.Vitality,
                Strength = startAttribute.Strength + AttributeIncreasePerLevel.Strength,
                Dexterity = startAttribute.Dexterity + AttributeIncreasePerLevel.Dexterity,
                Intelligence = startAttribute.Intelligence + AttributeIncreasePerLevel.Intelligence
            };
            // Act
            testCharacter.LevelUp();
            PrimaryAttribute actuel = testCharacter.TotalPrimaryAttributes;
            // Assert
            Assert.Equal(expected, actuel);
        }

        [Fact]
        public void LevelUp_CharacterLevelUp_SecondaryAttributesIncreaseAsExpected()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            SecondaryAttribute expected = new SecondaryAttribute() { Health = 150, ArmorRating = 12, ElementalResistance = 2 };
            // Act
            testCharacter.LevelUp();
            SecondaryAttribute actual = testCharacter.SecondaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
