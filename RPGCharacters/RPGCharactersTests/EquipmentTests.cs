﻿using System;
using Xunit;
using RPGCharacters.Characters;
using RPGCharacters.Attribute;
using RPGCharacters.Items;
using RPGCharacters.Exceptions;

namespace RPGCharactersTests
{
    public class EquipmentTests
    {

        [Fact]
        public void EquipItem_CharacterEquipWeaponWithTooHighLevel_ThrowInvalidWeaponException()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 2,
                ItemSlot = Slots.SLOT_WEAPON,
                WeaponType = WeaponTypes.WEAPON_AXE,
                Attributes = new WeaponAttribute() { Damage = 7, AttackSpeed = 1.1 }
            };

            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => testCharacter.EquipItem(testAxe));
        }

        [Fact]
        public void EquipItem_CharacterEquipArmorWithTooHighLevel_ThrowInvalidArmorException()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = Slots.SLOT_BODY,
                ArmorType = ArmorTypes.ARMOUR_PLATE,
                Attributes = new PrimaryAttribute() { Vitality = 2, Strength = 1 }
            };

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => testCharacter.EquipItem(testPlateBody));
        }

        [Fact]
        public void EquipItem_CharacterEquipWrongWeaponType_ThrowInvalidWeaponException()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slots.SLOT_WEAPON,
                WeaponType = WeaponTypes.WEAPON_BOW,
                Attributes = new WeaponAttribute() { Damage = 12, AttackSpeed = 0.8 }
            };

            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => testCharacter.EquipItem(testBow));
        }

        [Fact]
        public void EquipItem_CharacterEquipWrongArmorType_ThrowInvalidArmorException()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = Slots.SLOT_HEAD,
                ArmorType = ArmorTypes.ARMOUR_CLOTH,
                Attributes = new PrimaryAttribute() { Vitality = 1, Intelligence = 5 }
            };

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => testCharacter.EquipItem(testClothHead));
        }

        [Fact]
        public void EquipItem_CharacterEquipsValidWeapon_ReturnSuccessMessage()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slots.SLOT_WEAPON,
                WeaponType = WeaponTypes.WEAPON_AXE,
                Attributes = new WeaponAttribute() { Damage = 12, AttackSpeed = 0.8 }
            };
            string expected = "New weapon equipped!";
            // Act
            String actual = testCharacter.EquipItem(testAxe);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipItem_CharacterEquipsValidArmor_ReturnSuccessMessage()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slots.SLOT_BODY,
                ArmorType = ArmorTypes.ARMOUR_PLATE,
                Attributes = new PrimaryAttribute() { Vitality = 2, Strength = 1 }
            };
            string expected = "New armour equipped!";
            // Act
            String actual = testCharacter.EquipItem(testPlateBody);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDps_ForACharacterWithoutWeapon_ReturnTheDamagePerSecond()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            int strength = 5;
            double expected = 1*(1+((double) strength/ 100));
            // Act
            double actual = testCharacter.GetDps();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDps_ForACharacterWithWeapon_ReturnTheDamagePerSecond()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slots.SLOT_WEAPON,
                WeaponType = WeaponTypes.WEAPON_AXE,
                Attributes = new WeaponAttribute() { Damage = 12, AttackSpeed = 0.8 }
            };
            int strength = 5;
            double expected = (testAxe.Attributes.Damage * testAxe.Attributes.AttackSpeed) * (1 + ((double)strength / 100));
            // Act
            testCharacter.EquipItem(testAxe);
            double actual = testCharacter.GetDps();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDps_ForACharacterWithWeaponAndArmor_ReturnTheDamagePerSecond()
        {
            // Arrange
            Character testCharacter = new Warrior("test");
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slots.SLOT_WEAPON,
                WeaponType = WeaponTypes.WEAPON_AXE,
                Attributes = new WeaponAttribute() { Damage = 12, AttackSpeed = 0.8 }
            };
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slots.SLOT_BODY,
                ArmorType = ArmorTypes.ARMOUR_PLATE,
                Attributes = new PrimaryAttribute() { Vitality = 2, Strength = 1 }
            };
            int strength = 5;
            double expected = (testAxe.Attributes.Damage * testAxe.Attributes.AttackSpeed) * (1 + ((double) (strength + testPlateBody.Attributes.Strength) / 100));
            // Act
            testCharacter.EquipItem(testAxe);
            testCharacter.EquipItem(testPlateBody);
            double actual = testCharacter.GetDps();
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
